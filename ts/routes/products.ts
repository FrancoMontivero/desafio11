import express from 'express';

const routerProducts = express.Router();

let array: any[] = [];
let currentId: number = 0;

routerProducts.get('/listar', (req, res) => {
	if (array.length) res.json(array);
	else res.json({error: "No hay productos cargados"});
})

routerProducts.get('/listar/:id', (req, res, next) => {
	try {
		const id: number = parseInt(req.params.id);
		if(isNaN(id)) res.json({error: "El id del produto debe ser un caracter numerico"})
		else {
			let index = array.findIndex(e => e.id === id);
			if(index === -1) res.json({error: "Producto no encontrado"})
			else res.json(array[index])
		}
	}
	catch (err) {
		next(err);
	}
})

routerProducts.post('/guardar', (req, res) => {
	const {
		title,
		price,
		thumbnail
	} = req.body;
	
	if(!(title && price && thumbnail)) {
		res.json({error: "Hay parametros vacios o indefinidos"});
	}
	else if(isNaN(price)) {
		res.json({error: "El precio no puede contener caracteres no numericos"});
	}
	else {
		currentId++
		const newProduct = {id: currentId, title, price: parseFloat(price), thumbnail};
		
		array.push(newProduct);
		res.json(newProduct);
	}

})

routerProducts.put('/actualizar/:id', (req, res) => {
	const {
		title,
		price,
		thumbnail
	} = req.body;

	const id: number = parseInt(req.params.id);

	if(isNaN(id)) res.json({error: "El id del producto debe ser un numero"})
	else {
		let index: number = array.findIndex(e => e.id === id)
		if(index === -1) {
			res.json({error: "No existe un producto con el id " + id});	
		}else {
			if(!(title && price && thumbnail)) {
				res.json({error: "Hay parametros vacios o indefinidos"});
			}
			else {
				const newProduct = {id, title, price, thumbnail};
				array[index] = newProduct;
				res.json(newProduct);
			}
		}
	}
})

routerProducts.delete('/borrar/:id', (req, res) => {

	const id: number = parseInt(req.params.id);

	if(isNaN(id)) res.json({error: "El id del producto debe ser un numero"})
	else {	
		let index: number = array.findIndex(e => e.id === id);
		if(index === -1) {
			res.json({error: "No existe un producto con el id " + id});	
		}else {
			const aux = array[index];
			array.splice(index, 1);
			res.json(aux);
		}
	}
})

routerProducts.get('/vista', (req, res) => {
	res.render('products', {products: array});
})

export default routerProducts;