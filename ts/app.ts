import express from 'express';

import path from 'path';
import cors from './middlewares/cors';
import router from './routes/index';
import morgan from './middlewares/morgan'
import multer from './middlewares/multer';
import errorHandler from './middlewares/errorHandler';

import { PORT, VIEW_ENGINE } from './config';

const app = express();

app.use(express.json());
app.use(multer.array('any'));
app.use(express.urlencoded({ extended : true }));
app.use(express.static(path.resolve(__dirname, '../assets')));

app.set('view engine', VIEW_ENGINE);
app.set('views', path.resolve(__dirname, '../', 'views'));

app.use(cors);
app.use(morgan);

app.use(router);
app.use(errorHandler);

app.set('port', PORT);

export default app;