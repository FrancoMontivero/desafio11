# Desafío 11 - EJS Y PUG
_desafío 11 de Franco Montivero_

## ⚙️ Instalación y pruebas
Primero instalar todos las dependencias del proyecto
```
npm install
```
### Variables de entorno
Para poder hacer pruebas en el ambiente de desarrollo, se puede crear un archivo development.env que sirve para poder definir variables de entorno, el archivo developmentExample.env contiene un ejemplo de las variables de entorno que puedes usar.  
  
Los valores por defecto para el ambiente de desarrollo son los siguientes:
```
PORT=8080
ORIGIN_CLIENT='http://localhost:8081'
VIEW_ENGINE === 'ejs'
```
### Scripts
En el archivo package.json se definen los siguientes scripts:
* **tsc**: transpila los archivos typescript que se encuentran en el directorio*"tsc"* al directorio *"src"*.
* **tsc:watcher**: transpila los archivos typescript que se encuentran en el directorio *"tsc"* al directorio *"src"* y se queda esperando cambios para volver a transpilar.
* **tsc:build-index**: transpilar el archivo index.ts que se encuentra en la raíz del desafío.
* **start:dev**: levanta el servidor en un ambiente de desarrollo y por defecto con el motor de vistas ejs.
* **start:dev -pug**: levanta el servidor en un ambiente de desarrollo y con el motor de vistas pug.
* **start:dev -watcher**: levanta el servidor en un ambiente de desarrollo, quedando a la escucha de cambios y aplicando estos cambios al servidor de forma automática.
* **start:dev -pug -watcher**: levanta el servidor en un ambiente de desarrollo, quedando a la escucha de cambios y aplicando estos cambios al servidor de forma automática, con el motor de vistas pug.
* **test**: ejecuta los test que se encuentra en el directorio *__ test __*

### Pruebas para motores de vista
Para porder probar un motor de vista determinado, hay 2 formas:
#### En la rama master  
La primera forma es ejecutando:  
```
npm run "start:dev -pug"
```
En caso de necesitar un watcher:
```
npm run "start:dev -pug -watcher"
```
(Tambien se puede agregar una variable de entorno en el archivo development.env)
#### Alternativa
El desafío tiene 3 ramas creadas:  
![Ramas del desafío](./img/branches.png)  
La rama **master**, en donde para escoger el motor de vistas, se realiza mediante variables de entorno y otras 2 ramas con el nombre del motor de vistas que usa cada rama.

## 📜 Documentación de la API 

| Método |     Recurso   |  Descripción  |
|  ----- | ------------- | ------------- |
| GET    | /api/productos/listar  | Permite Listar todos los productos cargados o devuelve un error en caso de no haber productos.  |
| GET    | /api/productos/listar/*:id*| Si el parámetro *id* es un carácter numérico y existe un producto con ese id, devuelve el producto|
| GET    | /api/productos/vista | Muestra una vista para todos los productos cargados |
| POST   | /api/productos/guardar | Permite cargar un producto.  |
| PUT    | /api/productos/actualizar/*:id* | Si el parámetro *id* es un carácter numérico y existe un producto con ese id, permite actualizar la información del producto con ese id.|
| DELETE | /api/productos/borrar/*:id*  | Si el parámetro *id* es un carácter numérico y existe un producto con ese id, permite borrar el producto con ese id. |

Tomando como base que se encuentren cargados los siguientes productos:
```
[
    {
        "id":1,
        "title":"Escuadra",
        "price":123.45,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
    },
    {
        "id":2,
        "title":"Calculadora",
        "price":234.56,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/calculator-math-tool-school-256.png"
    },
    {
        "id":3,
        "title":"Globo Terráqueo",
        "price":345.67,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/globe-earth-geograhy-planet-school-256.png"
    }
]
```
**Se describen los siguientes endpoints:**

### Obtener los productos
Esta llamada permite obtener todos los productos cargados.  

Llamada:
```
curl -X GET http://localhost:$PORT/api/productos/listar
```
Ejemplo:
```
curl -X GET http://localhost:8080/api/productos/listar
```
Respuesta:
```
[
    {
        "id":1,
        "title":"Escuadra",
        "price":123.45,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
    },
    {
        "id":2,
        "title":"Calculadora",
        "price":234.56,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/calculator-math-tool-school-256.png"
    },
    {
        "id":3,
        "title":"Globo Terráqueo",
        "price":345.67,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/globe-earth-geograhy-planet-school-256.png"
    }
]
```
Respuesta en caso de que no haya productos cargados:
```
{
    "error": "No hay productos cargados"
}
```

### Obtener un producto por su id
Esta llamada permite obtener un producto por su id.  

Llamada:
```
curl -X GET http://localhost:$PORT/api/productos/listar/$ID
```
Ejemplo:
```
curl -X GET http://localhost:8080/api/productos/listar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "Producto no encontrado"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del produto debe ser un caracter numerico"
}
```

### Cargar un producto
Esta llamada permite cargar un producto nuevo.  

Llamada:
```
curl -X POST -H "Content-Type: application/json" -d '{
    "title": "Some title",
    "price": 1111,
    "thumbnail": "Some url"
}'
http://localhost:$PORT/api/productos/guardar
```
Ejemplo:
```
curl -X POST -H "Content-Type: application/json" -d '{
    "title": "Escuadra",
    "price": 123.45,
    "thumbnail": "https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}'
http://localhost:8080/api/productos/guardar
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que falten parametros:
```
{
    "error": "Hay parametros vacios o indefinidos"
}
```
Respuesta en caso de que el precio no sea un numero:
```
{
    "error": "El precio no puede contener caracteres no numericos"
}
```

### Actualizar un producto
Esta llamada permite actualizar con su id, un producto existente.  

Llamada:
```
curl -X PUT -H "Content-Type: application/json" -d '{
    "title": "New title",
    "price": 2222,
    "thumbnail": "New url"
}'
http://localhost:$PORT/api/productos/actualizar/$ID
```
Ejemplo:
```
curl -X PUT -H "Content-Type: application/json" -d '{
    "title": "Nueva escuadra",
    "price": 123.45,
    "thumbnail": "https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}'
http://localhost:8080/api/productos/actualizar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Nueva escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del produto debe ser un numero"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "No existe un producto con el id 1"
}
```
Respuesta en caso de que falten parametros:
```
{
    "error": "Hay parametros vacios o indefinidos"
}
```
Respuesta en caso de que el precio no sea un numero:
```
{
    "error": "El precio no puede contener caracteres no numericos"
}
```

### Borrar un producto
Esta llamada permite borrar con su id, un producto.  

Llamada:
```
curl -X DELETE http://localhost:$PORT/api/productos/borrar/$ID
```
Ejemplo: 
```
curl -X DELETE http://localhost:8080/api/productos/borrar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del produto debe ser un numero"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "No existe un producto con el id 1"
}
```

### Vista de productos
Esta llamada permite mostrar una vista de todo los productos utlizando el motor de vista ejs o pug

Llamada:
```
curl -X GET http://localhost:$PORT/api/productos/vista
```
Ejemplo:
```
curl -X GET http://localhost:8080/api/productos/vista
```
Respuesta:  
![Vista con productos cargados](./img/view-products.png)   

Respuesta en caso de que no haya productos cargados:  
![Vista sin productos cargados](./img/view-products-empty.png)  
## 🔧 Test
Para los test, ejecutar el siguiente comando:
```
npm run test
```