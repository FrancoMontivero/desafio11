const app = require('../src/app.js')['default'];
const supertest = require('supertest');

const PRFEIX_FILE_STATICS = '/'

const PREFIX = '/api/productos'
const PUT_PRODUCT = PREFIX + '/actualizar';
const POST_PRODUCT = PREFIX + '/guardar';
const GET_PRODUCTS = PREFIX + '/listar';
const DELETE_PRODUCT = PREFIX + '/borrar';


describe("request products", () => {
	const agent = supertest(app);
	const products = [
		{title: "Producto 1", price: 1000, thumbnail: 'some url'},
		{title: "Producto 2", price: 2000, thumbnail: 'some url'},
		{title: "Producto 3", price: 3000, thumbnail: "some url"},
		{title: "Producto 4", price: 4000, thumbnail: "some url"},
		{title: "Producto 5", price: 5000, thumbnail: "some url"},
	];
	
    it("GET /api/productos/listar (empty products)", async () => {
        let response = await agent.get(GET_PRODUCTS);
		expect(response.status).toBe(200);
		expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
		expect(response.body instanceof Object).toBeTruthy();
		expect(response.body.hasOwnProperty('error')).toBeTruthy();
		expect(response.body).toEqual({error: "No hay productos cargados"});
    })

	it('the response for the file static index.hmtl should correct', async () => {
		const response = await agent.get( PRFEIX_FILE_STATICS + 'index.html');
		expect(response.status).toBe(200);
		expect(response.headers['content-type']).toBe('text/html; charset=UTF-8');
		expect(response.text).not.toBe('');
	})

    describe('POST /api/productos/guardar', () => {
        it("the response should be a error when missing parameters", async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send({title: "some text",price: 32})

            expect(response.status).toBe(200);
            expect(response.body instanceof Object).toBeTruthy();
            expect(response.body.hasOwnProperty('error')).toBeTruthy()
            expect(response.body).toEqual({error: "Hay parametros vacios o indefinidos"});
            
        });

        it("the response should be a error when price has at least one non-numeric character", async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send({title: "some text", price: "201A", thumbnail: "some url"})

            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Object).toBeTruthy();
            expect(response.body.hasOwnProperty("error")).toBeTruthy();
            expect(response.body.error).toBe("El precio no puede contener caracteres no numericos");
        })

        it('the should be the product loaded', async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send(products[0]);

            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Object).toBeTruthy();
            expect(
                {title: response.body.title, price: response.body.price, thumbnail: response.body.thumbnail}
            )
            .toEqual(products[0]);
        });

        it("the should to assign the proper id", async () => {	

			for(let product of products.slice(1)) {
				await agent.post(POST_PRODUCT).send(product);
			}

            const response = await agent.get(GET_PRODUCTS);

            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Array).toBeTruthy();

			let isAssignProper = true;
			let i = 0;
			while(isAssignProper && i < products.length) {
				if(response.body[i].id === i + 1) i++;
				else isAssignProper = false;
			}
			
            expect(isAssignProper).toBeTruthy();
        });
    });
    
	describe('DELETE /api/productos/eliminar/:id', () => {
		const ID = 3;

		it("the response should be a error when the id is non-numeric character", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/a`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "El id del producto debe ser un numero"})
		})

		it("the response should be a error when the hasn't none product with that id", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/6`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "No existe un producto con el id 6"})
		})

		it("the response should be the product removed", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/${ID}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual({id: ID, ...products[ID - 1]});
		})

		it("the listing should not has the product removed", async () => {
			const response = await agent.get(`${GET_PRODUCTS}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Array).toBeTruthy();
			expect(response.body.find(e => e.id === ID)).toBeUndefined();
		})
	})

	describe(`PUT ${PUT_PRODUCT}/:id`, () => {
		const ID = 2;
		const newData = {
			title: "Producto 3.1",
			price: 123.45,
			thumbnail: "some url"
		};

		it('the response should be a error when the id is non-numeric character', async () => {
			const response = await agent.put(`${PUT_PRODUCT}/a`);
			expect(response.status).toBe(200);
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "El id del producto debe ser un numero"});
		})

		it("the response should be a error when hasn't none product with that id", async () => {
			const response = await agent.put(`${PUT_PRODUCT}/6`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "No existe un producto con el id " + 6})
		})

		it('the response should be the updated product', async () => {
			const response = await agent.put(`${PUT_PRODUCT}/${ID}`).send(newData)
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual({id: ID, ...newData});
		})

		it('the update product should appear in the listing', async () => {
			const response = await agent.get(`${GET_PRODUCTS}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Array).toBeTruthy();
			expect(response.body.find(e => e.id === ID)).not.toBeUndefined();
			expect(response.body.find(e => e.id === ID)).toEqual({id: ID, ...newData})

		})

		it('the update product should appear proper', async () => {
			const response = await agent.get(`${GET_PRODUCTS}/${ID}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual({id: ID, ...newData});
		})
	})
})
