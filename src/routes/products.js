"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var routerProducts = express_1.default.Router();
var array = [];
var currentId = 0;
routerProducts.get('/listar', function (req, res) {
    if (array.length)
        res.json(array);
    else
        res.json({ error: "No hay productos cargados" });
});
routerProducts.get('/listar/:id', function (req, res, next) {
    try {
        var id_1 = parseInt(req.params.id);
        if (isNaN(id_1))
            res.json({ error: "El id del produto debe ser un caracter numerico" });
        else {
            var index = array.findIndex(function (e) { return e.id === id_1; });
            if (index === -1)
                res.json({ error: "Producto no encontrado" });
            else
                res.json(array[index]);
        }
    }
    catch (err) {
        next(err);
    }
});
routerProducts.post('/guardar', function (req, res) {
    var _a = req.body, title = _a.title, price = _a.price, thumbnail = _a.thumbnail;
    if (!(title && price && thumbnail)) {
        res.json({ error: "Hay parametros vacios o indefinidos" });
    }
    else if (isNaN(price)) {
        res.json({ error: "El precio no puede contener caracteres no numericos" });
    }
    else {
        currentId++;
        var newProduct = { id: currentId, title: title, price: parseFloat(price), thumbnail: thumbnail };
        array.push(newProduct);
        res.json(newProduct);
    }
});
routerProducts.put('/actualizar/:id', function (req, res) {
    var _a = req.body, title = _a.title, price = _a.price, thumbnail = _a.thumbnail;
    var id = parseInt(req.params.id);
    if (isNaN(id))
        res.json({ error: "El id del producto debe ser un numero" });
    else {
        var index = array.findIndex(function (e) { return e.id === id; });
        if (index === -1) {
            res.json({ error: "No existe un producto con el id " + id });
        }
        else {
            if (!(title && price && thumbnail)) {
                res.json({ error: "Hay parametros vacios o indefinidos" });
            }
            else {
                var newProduct = { id: id, title: title, price: price, thumbnail: thumbnail };
                array[index] = newProduct;
                res.json(newProduct);
            }
        }
    }
});
routerProducts.delete('/borrar/:id', function (req, res) {
    var id = parseInt(req.params.id);
    if (isNaN(id))
        res.json({ error: "El id del producto debe ser un numero" });
    else {
        var index = array.findIndex(function (e) { return e.id === id; });
        if (index === -1) {
            res.json({ error: "No existe un producto con el id " + id });
        }
        else {
            var aux = array[index];
            array.splice(index, 1);
            res.json(aux);
        }
    }
});
routerProducts.get('/vista', function (req, res) {
    res.render('products', { products: array });
});
exports.default = routerProducts;
