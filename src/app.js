"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var path_1 = __importDefault(require("path"));
var cors_1 = __importDefault(require("./middlewares/cors"));
var index_1 = __importDefault(require("./routes/index"));
var morgan_1 = __importDefault(require("./middlewares/morgan"));
var multer_1 = __importDefault(require("./middlewares/multer"));
var errorHandler_1 = __importDefault(require("./middlewares/errorHandler"));
var config_1 = require("./config");
var app = express_1.default();
app.use(express_1.default.json());
app.use(multer_1.default.array('any'));
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.static(path_1.default.resolve(__dirname, '../assets')));
app.set('view engine', process.env.VIEW_ENGINE === 'pug' ? 'pug' : 'ejs');
app.set('views', path_1.default.resolve(__dirname, '../', 'views'));
app.use(cors_1.default);
app.use(morgan_1.default);
app.use(index_1.default);
app.use(errorHandler_1.default);
app.set('port', config_1.PORT);
exports.default = app;
